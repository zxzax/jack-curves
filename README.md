# jack-curves

![Preview](https://gitlab.com/meditator/jack-curves/raw/images/main.png)

Automates JACK MIDI ports with Bézier curves.

## Dependencies

* [CMake](https://cmake.org) (for building)
* [GTK+3](https://gtk.org)
* [JACK](https://jackaudio.org)

## Building

```shell
git clone --recursive https://gitlab.com/meditator/jack-curves
cd jack-curves
mkdir build
cd build
cmake ..
make
make install
```

## Guide

Currently this only works in tandem with applications that support JACK
transport. If you're using a DAW such as [Ardour](https://ardour.org), make sure
to enable external JACK position sync.

The length of the curve is determined by the start/end frames. To begin, move
the transport to the desired start position, then click the "Set" button at the
top. Repeat with the end position, then draw your curve. Curve editing commands:

* Left Click - Create/move point
* Right click - Remove point
* Shift + Left Click + Drag - Move point and control points simulaneously
* Ctrl + Left Click + Drag - Move symmetrically (only works on control points)

Decreasing the resolution will result in a finer curve. When automating the
pitch wheel, you will likely need to center your curve in the middle of the
grid.

To save presets, use a LADISH session manager (such as gladish or
[Claudia](http://kxstudio.linuxaudio.org/Applications:Claudia)) and run the
program inside a room. This program uses very little memory, so to have
more than one curve per session, you can run multiple instances of it.

## Roadmap

* External control via OSC.
* On-Demand/MIDI-triggered mode.
* Tabbed interface to support multiple curves.
* Horizontal movement of control points.

## License

[Apache 2.0](https://www.apache.org/licenses/LICENSE-2.0.html)

## Similar Applications

[Bezie](https://github.com/jperler/bezie)
