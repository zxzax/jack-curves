/*
 * Copyright 2019 Meditator
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <gtk/gtk.h>
#include <jack/jack.h>
#include <jack/midiport.h>
#include <jack/ringbuffer.h>
#include <jack/session.h>
#include <rtosc/rtosc.h>
#include <stdarg.h>
#include <math.h>
#include <errno.h>

#define MAX_POINTS 128

typedef struct {
  gdouble x, y;
  gdouble cl, cr;
} curve_point_t;

typedef enum {
  POINT_NULL = -1,
  POINT_POSITION = 0,
  POINT_CONTROL_LEFT,
  POINT_CONTROL_RIGHT
} point_type_t;

typedef struct {
  gint index;
  point_type_t type;
} curve_item_t;

#define BUFFER_SIZE (sizeof(curve_point_t) * MAX_POINTS * 8)

typedef struct {
  int argc;
  char **argv;
  jack_client_t *client;
  jack_port_t *port;
  jack_ringbuffer_t *buffer;
  GtkBuilder *builder;

  /* ui thread */
  GArray *edit_points;
  curve_item_t selected;
  curve_item_t hovered;
  gdouble play_position;
  gdouble last_play_position;
  gboolean handles_visible;
  gboolean snapping;
  guint x_grid;
  guint y_grid;

  /* process thread */
  jack_nframes_t time_start;
  jack_nframes_t time_end;
  /* -1    = pitch wheel
   * 0-127 = midi controller id */
  int controller;
  /* 0-15 = midi channel */
  int channel;
  /* interval at which midi messages should be sent */
  int resolution;
  /* curve points */
  curve_point_t points[MAX_POINTS];
  int num_points;
} jack_curves;

static char write_buffer[BUFFER_SIZE];
static void osc_post_message(
    jack_ringbuffer_t *buffer,
    const char *dest,
    const char *args,
    ...) {
  va_list va, va2;
  va_start(va, args);
  va_copy(va2, va);
  const size_t size = rtosc_vmessage(NULL, 0, dest, args, va);
  va_end(va);
  if (size < jack_ringbuffer_write_space(buffer)) {
    rtosc_vmessage(write_buffer, size, dest, args, va);
    jack_ringbuffer_write(buffer, write_buffer, size);
  }
  va_end(va2);
}

typedef struct {
  char *pattern;
  void (*func)(jack_curves *, const char *msg);
} dispatch_t;

#define DEF_SETTER(_name, _type) \
void set_##_name(jack_curves *curves, const char *msg) { \
  curves->_name = rtosc_argument(msg, 0)._type; \
}

DEF_SETTER(resolution, i)
DEF_SETTER(channel, i)
DEF_SETTER(controller, i)
DEF_SETTER(time_start, i)
DEF_SETTER(time_end, i)

static void set_points(jack_curves *curves, const char *msg) {
  rtosc_blob_t blob = rtosc_argument(msg, 0).b;
  size_t offset;
  curves->num_points = 0;
  while (offset + sizeof(curve_point_t) <= blob.len && curves->num_points < MAX_POINTS) {
    curves->points[curves->num_points] = *((curve_point_t *) (blob.data + offset));
    curves->num_points++;
    offset += sizeof(curve_point_t);
  }
}

static const dispatch_t dispatch_table[] = {
  {"/resolution:i", set_resolution},
  {"/channel:i", set_channel},
  {"/controller:i", set_controller},
  {"/time-start:i", set_time_start},
  {"/time-end:i", set_time_end},
  {"/points:b", set_points},
  {NULL, NULL}
};

static void write_midi_message(
    unsigned char msg[3],
    double value,
    unsigned channel,
    int controller) {
  value = CLAMP(value, 0., 1.);
  if (controller == -1) {
    msg[0] = 0xe0 | (channel & 0xf);
    unsigned v = ((unsigned)(value * 0x3fff)) & 0x3fff;
    msg[1] = v & 0x7f;
    msg[2] = (v >> 7) & 0x7f;
  } else {
    msg[0] = 0xb0 | (channel & 0xf);
    msg[1] = controller & 0x7f;
    msg[2] = ((unsigned)(value * 0x7f)) & 0x7f;
  }
}

static char read_buffer[BUFFER_SIZE];
static int process(jack_nframes_t nframes, void *arg) {
  jack_curves *curves = arg;
  size_t read_len;
  unsigned char midi_message[3];

  void * const port_buf = jack_port_get_buffer(curves->port, nframes);
  jack_midi_clear_buffer(port_buf);

  jack_latency_range_t latency_range;
  jack_port_get_latency_range(curves->port, JackPlaybackLatency, &latency_range);

  jack_position_t pos;
  jack_transport_state_t state = jack_transport_query(curves->client, &pos);
  const jack_nframes_t cur_frame = pos.frame - latency_range.max;
  const jack_nframes_t cur_frame_end = cur_frame + nframes;

  while ((read_len = jack_ringbuffer_read_space(curves->buffer)) > 0) {
    jack_ringbuffer_read(curves->buffer, read_buffer, read_len);
    size_t offset = 0;
    while (read_len > 0) {
      while (read_buffer[offset] != '/' && read_len > 0) {
        offset++;
        read_len--;
      }
      char *msg_ptr = read_buffer + offset;
      if (!strcmp(msg_ptr, "/send")) {
        write_midi_message(midi_message, curves->controller == -1 ? 0.5 : 0,
            curves->channel, curves->controller);
        jack_midi_event_write(port_buf, 0, midi_message, 3);
      } else {
        for (const dispatch_t *dispatch = dispatch_table;
            dispatch->pattern != NULL;
            dispatch++) {
          if (rtosc_match(dispatch->pattern, msg_ptr, NULL)) {
            dispatch->func(curves, msg_ptr);
            break;
          }
        }
      }
      const size_t msg_len = rtosc_message_length(msg_ptr, read_len);
      offset += msg_len;
      read_len -= msg_len;
    }
  }

  if (state == JackTransportRolling
      && curves->resolution > 0
      && curves->time_start <= curves->time_end
      && cur_frame_end >= curves->time_start /* check for 1d range overlap */
      && cur_frame <= curves->time_end) {


    const double win_size = curves->time_end - curves->time_start;
    const double end_x = (cur_frame - curves->time_start + nframes) / win_size;
    int left = 0;

    {
      const double sta_x = (cur_frame - ((double) curves->time_start)) / win_size;
      unsigned count = curves->num_points;
      /* binary search for the first point */
      while (count > 0) {
        int step = count / 2;
        unsigned search = left + step;
        if (!(sta_x < curves->points[search].x)) {
          left = search + 1;
          count -= step + 1;
        } else {
          count = step;
        }
      }
      if (left > 0) {
        left -= 1;
      }
    }
    for (int index = left;
        index < curves->num_points - 1 && curves->points[index].x < end_x;
        index++) {
      const jack_nframes_t begin = curves->points[index].x * win_size + curves->time_start;
      const jack_nframes_t end = curves->points[index + 1].x * win_size + curves->time_start;

      if (begin >= cur_frame && begin < cur_frame_end) {
        write_midi_message(midi_message, curves->points[index].y,
            curves->channel, curves->controller);
        /* g_print("%d <- [%02X, %02X, %02X]\n", begin, midi_message[0], midi_message[1], midi_message[2]); */
        jack_midi_event_write(port_buf, begin - cur_frame, midi_message, 3);
      }

      jack_nframes_t fi = begin - begin % curves->resolution;
      for (; fi < end && fi < cur_frame_end; fi += curves->resolution) {
        if (fi > begin && fi >= cur_frame) {
          const double yr = curves->points[index].y;
          const double cr = curves->points[index].cr;
          const double cl = curves->points[index + 1].cl;
          const double yl = curves->points[index + 1].y;
          const double t = (fi - begin) / ((double) (end - begin));
          const double u = 1. - t;
          const double y = yr * u*u*u + 3 * cr * u*u*t + 3 * cl * u*t*t + yl * t*t*t;
          write_midi_message(midi_message, y,
              curves->channel, curves->controller);
          /* g_print("%d <- [%02X, %02X, %02X]\n", fi, midi_message[0], midi_message[1], midi_message[2]); */
          jack_midi_event_write(port_buf, fi - cur_frame, midi_message, 3);
        }
      }
    }
    if (curves->num_points > 0) {
      const curve_point_t *last = &curves->points[curves->num_points - 1];
      const jack_nframes_t end = last->x * win_size + curves->time_start;
      if (end >= cur_frame && end < cur_frame_end) {
        write_midi_message(midi_message, last->y,
            curves->channel, curves->controller);
        /* g_print("%d <- [%02X, %02X, %02X]\n", end, midi_message[0], midi_message[1], midi_message[2]); */
        jack_midi_event_write(port_buf, end - cur_frame, midi_message, 3);
      }
    }
  }
  return 0;
}

typedef struct {
  jack_session_event_t *event;
  jack_curves* curves;
  GMutex mutex;
  GCond cond;
  gboolean done;
} curves_session_event_t;

#define KEY_GROUP "Curves"

static gboolean ui_session_event(void *user_data) {
  curves_session_event_t *data = user_data;
  jack_session_event_t *event = data->event;
  const jack_curves *curves = data->curves;

  event->flags = 0;
  if (event->type == JackSessionSave || event->type == JackSessionSaveAndQuit) {
    if (g_mkdir_with_parents(event->session_dir, 0600) == -1) {
      g_warning("%s", g_strerror(errno));
      event->flags = JackSessionSaveError;
    } else {
      g_autofree const char *state_file = g_build_filename(event->session_dir, "session.jcs", NULL);
      /* jack takes ownership of this string */
      event->command_line = g_strdup_printf("%s %s", curves->argv[0], state_file);

      g_autoptr(GKeyFile) key_file = g_key_file_new();
      g_autoptr(GError) error = NULL;

      g_key_file_set_string(key_file, KEY_GROUP, "uuid", event->client_uuid);
      g_key_file_set_double_list(key_file, KEY_GROUP, "points",
          (gdouble *) curves->edit_points->data, curves->edit_points->len * 4);
      g_key_file_set_integer(key_file, KEY_GROUP, "time_start", curves->time_start);
      g_key_file_set_integer(key_file, KEY_GROUP, "time_end", curves->time_end);
      g_key_file_set_integer(key_file, KEY_GROUP, "controller", curves->controller);
      g_key_file_set_integer(key_file, KEY_GROUP, "channel", curves->channel);
      g_key_file_set_integer(key_file, KEY_GROUP, "resolution", curves->resolution);
      g_key_file_set_boolean(key_file, KEY_GROUP, "snap", curves->snapping);
      g_key_file_set_integer(key_file, KEY_GROUP, "grid_x", curves->x_grid);
      g_key_file_set_integer(key_file, KEY_GROUP, "grid_y", curves->y_grid);

      if (!g_key_file_save_to_file(key_file, state_file, &error)) {
        g_warning("%s", error->message);
        event->flags = JackSessionSaveError;
      } else {
        g_print("Saved session to %s\n", state_file);
      }
    }
  }
  if (event->type == JackSessionSaveAndQuit) {
    g_application_quit(g_application_get_default());
  }
  g_mutex_lock(&data->mutex);
  data->done = TRUE;
  g_cond_signal(&data->cond);
  g_mutex_unlock(&data->mutex);
  return FALSE;
}

static void session(jack_session_event_t *event, void *arg) {
  curves_session_event_t data = {
    .event = event,
    .curves = arg,
    .done = FALSE
  };
  g_mutex_init(&data.mutex);
  g_cond_init(&data.cond);

  g_mutex_lock(&data.mutex);
  g_main_context_invoke_full(NULL, G_PRIORITY_HIGH, ui_session_event, &data, NULL);
  while (!data.done) {
    g_cond_wait(&data.cond, &data.mutex);
  }
  g_mutex_unlock(&data.mutex);

  jack_session_reply(data.curves->client, event);
  jack_session_event_free(event);
}

static gboolean curve_editor_draw(GtkWidget *widget, cairo_t *cr, const jack_curves *curves) {
  GtkStyleContext *style_ctx = gtk_widget_get_style_context(widget);
  int width = gtk_widget_get_allocated_width(widget);
  int height = gtk_widget_get_allocated_height(widget);

  GdkRGBA fg, grid, cursor;
  gtk_style_context_lookup_color(style_ctx, "theme_fg_color", &fg);
  gtk_style_context_lookup_color(style_ctx, "insensitive_bg_color", &grid);
  gtk_style_context_lookup_color(style_ctx, "insensitive_fg_color", &cursor);

  gtk_render_background(style_ctx, cr, 0, 0, width, height);
  cairo_set_line_width(cr, 1.);

  /* Grid */
  gdk_cairo_set_source_rgba(cr, &grid);
  for (int xi = 1; xi < curves->x_grid; xi++) {
    double x = xi / ((double) curves->x_grid) * width;
    cairo_move_to(cr, x, 0.);
    cairo_line_to(cr, x, height);
    cairo_stroke(cr);
  }
  for (int yi = 1; yi < curves->y_grid; yi++) {
    double y = yi / ((double) curves->y_grid) * height;
    cairo_move_to(cr, 0., y);
    cairo_line_to(cr, width, y);
    cairo_stroke(cr);
  }

  /* Transport Cursor */
  gdk_cairo_set_source_rgba(cr, &cursor);
  if (curves->play_position >= 0. && curves->play_position < 1.) {
    double x = curves->play_position * width;
    cairo_move_to(cr, x, 0.);
    cairo_line_to(cr, x, height);
    cairo_stroke(cr);
  }

  /* Curve line */
  gdk_cairo_set_source_rgba(cr, &fg);
  if (curves->edit_points->len > 0) {
    const curve_point_t *pa = &g_array_index(curves->edit_points, curve_point_t, 0);
    cairo_move_to(cr, pa->x * width, pa->y * height);
    for (guint i = 1; i < curves->edit_points->len; i++) {
      const curve_point_t *pb = &g_array_index(curves->edit_points, curve_point_t, i);
      gdouble diff = pb->x - pa->x;
      cairo_curve_to(cr,
          (pa->x + diff * (1/3.)) * width, pa->cr * height,
          (pa->x + diff * (2/3.)) * width, pb->cl * height,
          pb->x * width, pb->y * height);
      pa = pb;
    }
  }
  cairo_stroke(cr);

  /* Curve points and control handles */
  if (curves->handles_visible) {
    gdk_cairo_set_source_rgba(cr, &cursor);
    for (guint i = 0; i < curves->edit_points->len; i++) {
      const curve_point_t *p = &g_array_index(curves->edit_points, curve_point_t, i);
      double px = p->x * width;
      double py = p->y * height;
      double lx;
      double ly;
      double rx;
      double ry;
      if (i > 0) {
        const curve_point_t *prev = &g_array_index(curves->edit_points, curve_point_t, i - 1);
        lx = (prev->x + (p->x - prev->x) * (2/3.)) * width;
        ly = p->cl * height;
        cairo_move_to(cr, px, py);
        cairo_line_to(cr, lx, ly);
        cairo_stroke(cr);
      }
      if (i < curves->edit_points->len - 1) {
        const curve_point_t *next = &g_array_index(curves->edit_points, curve_point_t, i + 1);
        rx = (p->x + (next->x - p->x) * (1/3.)) * width;
        ry = p->cr * height;
        cairo_move_to(cr, px, py);
        cairo_line_to(cr, rx, ry);
        cairo_stroke(cr);
      }
      cairo_rectangle(cr, px - 2.5, py - 2.5, 5., 5.);
      if (i == curves->hovered.index && curves->hovered.type == POINT_POSITION) {
        cairo_fill_preserve(cr);
      }
      cairo_stroke(cr);
      if (i > 0) {
        ly = CLAMP(p->cl, 0., 1.) * height;
        cairo_arc(cr, lx, ly, 2.5, 0., 2 * M_PI);
        if (i == curves->hovered.index && curves->hovered.type == POINT_CONTROL_LEFT) {
          cairo_fill_preserve(cr);
        }
        cairo_stroke(cr);
      }
      if (i < curves->edit_points->len - 1) {
        ry = CLAMP(p->cr, 0., 1.) * height;
        cairo_arc(cr, rx, ry, 2.5, 0., 2 * M_PI);
        if (i == curves->hovered.index && curves->hovered.type == POINT_CONTROL_RIGHT) {
          cairo_fill_preserve(cr);
        }
        cairo_stroke(cr);
      }
    }
  }
  return TRUE;
}

static void curves_clamp_point(GArray *points, guint index) {
  curve_point_t *p = &g_array_index(points, curve_point_t, index);
  p->x = CLAMP(p->x, 0., 1.);
  p->y = CLAMP(p->y, 0., 1.);
  if (index > 0) {
    curve_point_t *prev = &g_array_index(points, curve_point_t, index - 1);
    p->x = CLAMP(p->x, prev->x, p->x);
  }
  if (index < points->len - 1) {
    curve_point_t *next = &g_array_index(points, curve_point_t, index + 1);
    p->x = CLAMP(p->x, p->x, next->x);
  }
}

static int curves_add_point(GArray *points, double mx, double my) {
  if (points->len >= MAX_POINTS) {
    return -1;
  }
  gint i;
  for (i = 0; i < points->len; i++) {
    const curve_point_t *p = &g_array_index(points, curve_point_t, i);
    if (p->x >= mx) {
      break;
    }
  }
  const curve_point_t point = {
    .x = mx, .y = my,
    .cr = my, .cl = my,
  };
  g_array_insert_val(points, i, point);
  curves_clamp_point(points, i);
  return i;
}

#define CLICK_RAD 8
#define CLICK_RAD_SQ (CLICK_RAD * CLICK_RAD)
#define SNAP_DIST 8

static void curves_get_item(
    const GArray *points,
    double ex,
    double ey,
    double width,
    double height,
    curve_item_t *out) {
  for (guint i = 0; i < points->len; i++) {
    const curve_point_t *p = &g_array_index(points, curve_point_t, i);

    /* control points have priority */
    double ax;
    double ay;
    if (i > 0) {
      const curve_point_t *prev = &g_array_index(points, curve_point_t, i - 1);
      ax = (prev->x + (p->x - prev->x) * (2/3.)) * width - ex;
      /* ensure we can click control points even if off grid */
      ay = CLAMP(p->cl, 0., 1.) * height - ey;
      if (ax * ax + ay * ay <= CLICK_RAD_SQ) {
        out->index = i;
        out->type = POINT_CONTROL_LEFT;
        break;
      }
    }
    if (i < points->len - 1) {
      const curve_point_t *next = &g_array_index(points, curve_point_t, i + 1);
      ax = (p->x + (next->x - p->x) * (1/3.)) * width - ex;
      ay = CLAMP(p->cr, 0., 1.) * height - ey;
      if (ax * ax + ay * ay <= CLICK_RAD_SQ) {
        out->index = i;
        out->type = POINT_CONTROL_RIGHT;
        break;
      }
    }
    ax = p->x * width - ex;
    ay = p->y * height - ey;
    if (ax * ax + ay * ay <= CLICK_RAD_SQ) {
      out->index = i;
      out->type = POINT_POSITION;
      break;
    }
  }
}

static void points_changed(GArray *points, jack_ringbuffer_t *buffer) {
  rtosc_blob_t blob;
  blob.len = points->len * sizeof(curve_point_t);
  blob.data = g_malloc(blob.len);
  for (guint i = 0; i < points->len; i++) {
    const curve_point_t *in = &g_array_index(points, curve_point_t, i);
    curve_point_t *out = (void *) &blob.data[i * sizeof(curve_point_t)];
    out->x = in->x;
    /* y values are inverted due to left-handed drawing coords */
    out->y = 1. - in->y;
    out->cr = 1. - in->cr;
    out->cl = 1. - in->cl;
  }
  osc_post_message(buffer, "/points", "b", blob);
  g_free(blob.data);
}

static gboolean curve_editor_click(GtkWidget *widget, GdkEventButton *event, jack_curves *curves) {
  double width = gtk_widget_get_allocated_width(widget);
  double height = gtk_widget_get_allocated_height(widget);

  GArray *points = curves->edit_points;

  if (event->button == 1) {
    curves_get_item(points, event->x, event->y, width, height, &curves->selected);
    if (curves->selected.index == -1) {
      double mx = event->x / width;
      mx = CLAMP(mx, 0., 1.);
      double my = event->y / height;
      my = CLAMP(my, 0., 1.);
      if (curves->snapping) {
        const double x_grid = curves->x_grid;
        const double y_grid = curves->y_grid;
        const int esx = round(mx * x_grid) / x_grid * width;
        const int esy = round(my * y_grid) / y_grid * height;
        if (fabs(esx - mx * width) < SNAP_DIST) {
          mx = esx / width;
        }
        if (fabs(esy - my * height) < SNAP_DIST) {
          my = esy / height;
        }
      }
      curves->selected.index = curves_add_point(curves->edit_points, mx, my);
      if (curves->selected.index != -1) {
        curves->selected.type = POINT_POSITION;
      }
      points_changed(curves->edit_points, curves->buffer);
    }
    gtk_widget_queue_draw(widget);
  } else if (event->button == 3) {
    for (guint i = 1; i < points->len - 1; i++) {
      const curve_point_t *p = &g_array_index(points, curve_point_t, i);
      const double ax = p->x * width - event->x;
      const double ay = p->y * height - event->y;
      if (ax * ax + ay * ay <= CLICK_RAD_SQ) {
        g_array_remove_index(curves->edit_points, i);
        points_changed(curves->edit_points, curves->buffer);
        gtk_widget_queue_draw(widget);
        break;
      }
    }
  }
  return TRUE;
}

static gboolean curve_editor_release(GtkWidget *widget, GdkEventButton *event, jack_curves *curves) {
  curves->selected.index = -1;
  curves->selected.type = POINT_NULL;
  return TRUE;
}

static gboolean curve_editor_enter(GtkWidget *widget, GdkEventCrossing *event, jack_curves *curves) {
  curves->handles_visible = TRUE;
  gtk_widget_queue_draw(widget);
  return TRUE;
}

static gboolean curve_editor_leave(GtkWidget *widget, GdkEventCrossing *event, jack_curves *curves) {
  curves->handles_visible = FALSE;
  gtk_widget_queue_draw(widget);
  return TRUE;
}

static gboolean curve_editor_motion(GtkWidget *widget, GdkEventMotion *event, jack_curves *curves) {
  double width = gtk_widget_get_allocated_width(widget);
  double height = gtk_widget_get_allocated_height(widget);

  curve_item_t last_hovered = curves->hovered;
  curves->hovered.index = -1;
  curves->hovered.type = POINT_NULL;
  curves_get_item(curves->edit_points, event->x, event->y, width, height, &curves->hovered);
  if (last_hovered.index != curves->hovered.index || last_hovered.type != curves->hovered.type) {
    gtk_widget_queue_draw(widget);
  }

  GArray *points = curves->edit_points;
  gint index = curves->selected.index;

  if (index > -1 && (event->state & GDK_BUTTON1_MASK)) {
    double mx = event->x / width;
    double my = event->y / height;
    curve_point_t *p = &g_array_index(points, curve_point_t, index);
    const double x_grid = curves->x_grid;
    const double y_grid = curves->y_grid;

    if (curves->selected.type == POINT_POSITION) {
      double ox = p->x;
      double oy = p->y;
      p->y = my;
      if (index > 0 && index < points->len - 1) {
        p->x = mx;
      }
      if (curves->snapping) {
        const int esx = round(p->x * x_grid) / x_grid * width;
        const int esy = round(p->y * y_grid) / y_grid * height;
        if (fabs(esx - p->x * width) < SNAP_DIST) {
          p->x = esx / width;
        }
        if (fabs(esy - p->y * height) < SNAP_DIST) {
          p->y = esy / height;
        }
      }
      curves_clamp_point(points, index);
      double ax = p->x - ox;
      double ay = p->y - oy;
      if (event->state & GDK_SHIFT_MASK) {
        p->cl += ay;
        p->cr += ay;
      }
      curves_clamp_point(points, index);
      if (ax || ay) {
        points_changed(points, curves->buffer);
      }
    } else if (curves->selected.type == POINT_CONTROL_LEFT && index > 0) {
      double oy = p->cl;
      p->cl = my;
      if (curves->snapping) {
        int esy = round(p->y * y_grid) / y_grid * height;
        if (fabs(esy - p->cl * height) < SNAP_DIST) {
          p->cl = esy / height;
        }
      }
      curves_clamp_point(points, index);
      double ay = p->cl - oy;
      if (event->state & GDK_SHIFT_MASK) {
        p->y += ay;
        p->cr += ay;
      } else if (event->state & GDK_CONTROL_MASK) {
        if (index > 0 && index < points->len - 1) {
          curve_point_t *prev = &g_array_index(points, curve_point_t, index - 1);
          curve_point_t *next = &g_array_index(points, curve_point_t, index + 1);
          gdouble dr = p->x - prev->x;
          gdouble dl = next->x - p->x;
          if (dr > 0) {
            p->cr = p->y - (p->cl - p->y) * (dl / dr);
          }
        }
      }
      curves_clamp_point(points, index);
      if (ay) {
        points_changed(points, curves->buffer);
      }
    } else if (curves->selected.type == POINT_CONTROL_RIGHT && index < points->len - 1) {
      double oy = p->cr;
      p->cr = my;
      if (curves->snapping) {
        int esy = round(p->y * y_grid) / y_grid * height;
        if (fabs(esy - p->cr * height) < SNAP_DIST) {
          p->cr = esy / height;
        }
      }
      curves_clamp_point(points, index);
      double ay = p->cr - oy;
      if (event->state & GDK_SHIFT_MASK) {
        p->y += ay;
        p->cl += ay;
      } else if (event->state & GDK_CONTROL_MASK) {
        if (index > 0 && index < points->len - 1) {
          curve_point_t *prev = &g_array_index(points, curve_point_t, index - 1);
          curve_point_t *next = &g_array_index(points, curve_point_t, index + 1);
          gdouble dr = p->x - prev->x;
          gdouble dl = next->x - p->x;
          if (dl > 0) {
            p->cl = p->y - (p->cr - p->y) * (dr / dl);
          }
        }
      }
      curves_clamp_point(points, index);
      if (ay) {
        points_changed(points, curves->buffer);
      }
    }
    gtk_widget_queue_draw(widget);
  }
  return TRUE;
}

#define DEF_CHANGED(_name, _path) \
static void _name##_changed(GtkWidget *widget, gpointer user_data) { \
  const jack_curves *curves = user_data; \
  gint value = gtk_spin_button_get_value(GTK_SPIN_BUTTON(widget)); \
  osc_post_message(curves->buffer, _path, "i", value); \
}

DEF_CHANGED(message_resolution, "/resolution")
DEF_CHANGED(channel, "/channel")
DEF_CHANGED(controller, "/controller")

static void time_start_changed(GtkWidget *widget, gpointer user_data) {
  const jack_curves *curves = user_data;
  gint value = gtk_spin_button_get_value(GTK_SPIN_BUTTON(widget));
  osc_post_message(curves->buffer, "/time-start", "i", value);
  GObject *end_widget = gtk_builder_get_object(curves->builder, "time-end");
  jack_nframes_t end = gtk_spin_button_get_value(GTK_SPIN_BUTTON(end_widget));
  if (value > end) {
    gtk_spin_button_set_value(GTK_SPIN_BUTTON(end_widget), value);
  }
}

static void time_end_changed(GtkWidget *widget, gpointer user_data) {
  const jack_curves *curves = user_data;
  gint value = gtk_spin_button_get_value(GTK_SPIN_BUTTON(widget));
  osc_post_message(curves->buffer, "/time-end", "i", value);
  GObject *start_widget = gtk_builder_get_object(curves->builder, "time-start");
  jack_nframes_t start = gtk_spin_button_get_value(GTK_SPIN_BUTTON(start_widget));
  if (value < start) {
    gtk_spin_button_set_value(GTK_SPIN_BUTTON(start_widget), value);
  }
}

static gboolean label_tick(GtkWidget *widget, GdkFrameClock *frame_clock, gpointer user_data) {
  jack_curves *curves = user_data;
  jack_position_t pos;
  jack_transport_query(curves->client, &pos);
  gchar *str = g_strdup_printf("%d", pos.frame);
  gtk_label_set_text(GTK_LABEL(widget), str);
  g_free(str);

  const GObject *start_widget = gtk_builder_get_object(curves->builder, "time-start");
  const gdouble start = gtk_spin_button_get_value(GTK_SPIN_BUTTON(start_widget));
  const GObject *end_widget = gtk_builder_get_object(curves->builder, "time-end");
  const gdouble end = gtk_spin_button_get_value(GTK_SPIN_BUTTON(end_widget));
  const gdouble region_width = end - start;

  curves->last_play_position = curves->play_position;
  if (region_width > 0.) {
    curves->play_position = (pos.frame - start) / region_width;
    if (curves->play_position < 0. || curves->play_position >= 1.) {
      curves->play_position = -1;
    }
  } else {
    curves->play_position = -1;
  }
  if (curves->play_position != curves->last_play_position) {
    gtk_widget_queue_draw(GTK_WIDGET(gtk_builder_get_object(curves->builder, "curve-editor")));
  }
  return G_SOURCE_CONTINUE;
}

static void set_time_start_clicked(GtkWidget *button, gpointer user_data) {
  jack_curves *curves = user_data;
  jack_position_t pos;
  jack_transport_query(curves->client, &pos);
  GObject *start_widget = gtk_builder_get_object(curves->builder, "time-start");
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(start_widget), pos.frame);
}

static void set_time_end_clicked(GtkWidget *button, gpointer user_data) {
  jack_curves *curves = user_data;
  jack_position_t pos;
  jack_transport_query(curves->client, &pos);
  GObject *end_widget = gtk_builder_get_object(curves->builder, "time-end");
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(end_widget), pos.frame);
}

static void grid_changed(GtkWidget *widget, gpointer user_data) {
  jack_curves *curves = user_data;
  curves->x_grid = gtk_spin_button_get_value(GTK_SPIN_BUTTON(gtk_builder_get_object(curves->builder, "grid-x")));
  curves->y_grid = gtk_spin_button_get_value(GTK_SPIN_BUTTON(gtk_builder_get_object(curves->builder, "grid-y")));
  gtk_widget_queue_draw(GTK_WIDGET(gtk_builder_get_object(curves->builder, "curve-editor")));
}

static void snap_toggled(GtkWidget *button, gpointer user_data) {
  jack_curves *curves = user_data;
  curves->snapping = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(button));
}

static void reset_curves_clicked(GtkWidget *button, gpointer user_data) {
  jack_curves *curves = user_data;
  if (curves->edit_points != NULL) {
    g_array_free(curves->edit_points, FALSE);
  }
  curves->edit_points = g_array_new(FALSE, FALSE, sizeof(curve_point_t));
  curve_point_t point_a = {
    .x = 0., .y = 0.,
    .cl = 0., .cr = 1/3.
  };
  g_array_append_val(curves->edit_points, point_a);
  curve_point_t point_b = {
    .x = 1., .y = 1.,
    .cl = 2/3., .cr = 1.
  };
  g_array_append_val(curves->edit_points, point_b);
  points_changed(curves->edit_points, curves->buffer);
  if (curves->builder != NULL) {
    gtk_widget_queue_draw(GTK_WIDGET(gtk_builder_get_object(curves->builder, "curve-editor")));
  }
}

static void send_test_clicked(GtkWidget *button, gpointer user_data) {
  const jack_curves *curves = user_data;
  osc_post_message(curves->buffer, "/send", "");
}

static void pitch_wheel_toggled(GtkWidget *button, gpointer user_data) {
  const jack_curves *curves = user_data;
  gboolean active = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(button));
  if (active) {
    osc_post_message(curves->buffer, "/controller", "i", -1);
  }
}

static void control_change_toggled(GtkWidget *button, gpointer user_data) {
  const jack_curves *curves = user_data;
  gboolean active = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(button));
  GtkWidget *controller_id = GTK_WIDGET(gtk_builder_get_object(curves->builder, "midi-controller-id"));
  gtk_widget_set_sensitive(controller_id, active);
  if (active) {
    gint value = gtk_spin_button_get_value(GTK_SPIN_BUTTON(controller_id));
    osc_post_message(curves->buffer, "/controller", "i", value);
  }
}

#define ERROR_DIALOG(...) { \
    GtkWidget *_error_dialog = gtk_message_dialog_new(NULL, \
        0, \
        GTK_MESSAGE_ERROR, \
        GTK_BUTTONS_CLOSE, \
        __VA_ARGS__); \
    gtk_dialog_run(GTK_DIALOG (_error_dialog)); \
    gtk_widget_destroy(_error_dialog); \
  }

static void curves_create_jack_client(jack_curves *curves, const char *session_id) {
  jack_status_t status;
  if (session_id == NULL) {
    curves->client = jack_client_open("jack-curves", JackNullOption, &status);
  } else {
    curves->client = jack_client_open("jack-curves", JackSessionID, &status, session_id);
  }
  if (curves->client == NULL) {
    ERROR_DIALOG("jack_client_open() failed, status = 0x%2.0x", status);
    return;
  }

  if (jack_set_process_callback(curves->client, process, curves)) {
    ERROR_DIALOG("jack_set_process_callback() failed");
    return;
  }
  if (jack_set_session_callback(curves->client, session, curves)) {
    ERROR_DIALOG("jack_set_session_callback() failed");
    return;
  }
  curves->port = jack_port_register(curves->client, "MIDI Out", JACK_DEFAULT_MIDI_TYPE, JackPortIsOutput, 0);
  if (curves->port == NULL) {
    ERROR_DIALOG("jack_port_register() failed");
    return;
  }
  curves->buffer = jack_ringbuffer_create(BUFFER_SIZE);
  if (curves->buffer == NULL) {
    ERROR_DIALOG("jack_ringbuffer_create() failed");
    return;
  }
  if (jack_activate(curves->client)) {
    ERROR_DIALOG("jack_activate() failed");
    return;
  }
}

static void ui_setup(GtkApplication *app, jack_curves *curves) {
  GtkCssProvider *provider = gtk_css_provider_new();
  gtk_css_provider_load_from_resource(provider, "/style.css");
  gtk_style_context_add_provider_for_screen(gdk_screen_get_default(),
      GTK_STYLE_PROVIDER(provider), GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);

  GtkBuilder *builder = gtk_builder_new_from_resource("/main.ui");
  curves->builder = builder;
  GtkWidget *window = GTK_WIDGET(gtk_builder_get_object(builder, "main-window"));
  gtk_application_add_window(app, GTK_WINDOW(window));

  GtkWidget *label = GTK_WIDGET(gtk_builder_get_object(builder, "current-time"));
  gtk_widget_add_tick_callback(label, label_tick, curves, NULL);
  gtk_builder_add_callback_symbols(builder,
      "message-resolution-changed", G_CALLBACK(message_resolution_changed),
      "channel-changed", channel_changed,
      "controller-changed", controller_changed,
      "time-end-changed", time_end_changed,
      "time-start-changed", time_start_changed,
      "pitch-wheel-toggled", pitch_wheel_toggled,
      "control-change-toggled", control_change_toggled,
      "set-time-start-clicked", set_time_start_clicked,
      "set-time-end-clicked", set_time_end_clicked,
      "grid-x-changed", grid_changed,
      "grid-y-changed", grid_changed,
      "snap-toggled", snap_toggled,
      "reset-curves-clicked", reset_curves_clicked,
      "send-test-clicked", send_test_clicked,
      "curve-editor-draw", curve_editor_draw,
      "curve-editor-click", curve_editor_click,
      "curve-editor-release", curve_editor_release,
      "curve-editor-enter", curve_editor_enter,
      "curve-editor-leave", curve_editor_leave,
      "curve-editor-motion", curve_editor_motion,
      NULL);
  gtk_builder_connect_signals(builder, curves);
}

static void ui_activate(GtkApplication *app, gpointer user_data) {
  jack_curves *curves = user_data;
  curves_create_jack_client(curves, NULL);
  reset_curves_clicked(NULL, curves);
  ui_setup(app, curves);
}

static void ui_open(GtkApplication *application, gpointer files, gint n_files, gchar *hint, gpointer user_data) {
  jack_curves *curves = user_data;
  for (gint i = 0; i < n_files; i++) {
    GFile *file = ((GFile **) files)[i];
    const char *path = g_file_peek_path(file);
    if (path == NULL) {
      continue;
    }
    GError *error = NULL;
    g_autoptr(GKeyFile) key_file = g_key_file_new();

    if (!g_key_file_load_from_file(key_file, path, G_KEY_FILE_NONE, &error)) {
      g_warning("%s: %s", path, error->message);
      continue;
    }
    g_autofree gchar *session_id = g_key_file_get_string(key_file, KEY_GROUP, "uuid", &error);
    if (session_id == NULL) {
      g_warning("%s: %s", path, error->message);
      g_error_free(error);
      error = NULL;
    }
    curves_create_jack_client(curves, session_id);
    gsize num_point_elems;
    double *points = g_key_file_get_double_list(key_file, KEY_GROUP, "points", &num_point_elems, NULL);
    if (points != NULL && num_point_elems >= 8) {
      curves->edit_points = g_array_new(FALSE, FALSE, sizeof(curve_point_t));
      for (gsize pi = 0; pi < num_point_elems - 3 && pi < MAX_POINTS * 4; pi += 4) {
        curve_point_t point = {
          .x = points[pi], .y = points[pi + 1], .cl = points[pi + 2], .cr = points[pi + 3]
        };
        g_array_append_val(curves->edit_points, point);
      }
      points_changed(curves->edit_points, curves->buffer);
    } else {
      reset_curves_clicked(NULL, curves);
    }
    ui_setup(application, curves);

    gtk_spin_button_set_value(
        GTK_SPIN_BUTTON(gtk_builder_get_object(curves->builder, "time-start")),
        g_key_file_get_integer(key_file, KEY_GROUP, "time_start", NULL)
        );
    gtk_spin_button_set_value(
        GTK_SPIN_BUTTON(gtk_builder_get_object(curves->builder, "time-end")),
        g_key_file_get_integer(key_file, KEY_GROUP, "time_end", NULL)
        );
    gint controller = g_key_file_get_integer(key_file, KEY_GROUP, "controller", &error);
    if (error != NULL) {
      controller = -1;
      g_error_free(error);
    }
    if (controller == -1) {
      gtk_toggle_button_set_active(
          GTK_TOGGLE_BUTTON(gtk_builder_get_object(curves->builder, "midi-type-pitch-wheel")),
          TRUE);
    } else {
      gtk_toggle_button_set_active(
          GTK_TOGGLE_BUTTON(gtk_builder_get_object(curves->builder, "midi-type-control-change")),
          TRUE);
      gtk_spin_button_set_value(
          GTK_SPIN_BUTTON(gtk_builder_get_object(curves->builder, "midi-controller-id")),
          controller);
    }
    gtk_spin_button_set_value(
        GTK_SPIN_BUTTON(gtk_builder_get_object(curves->builder, "midi-channel")),
        g_key_file_get_integer(key_file, KEY_GROUP, "channel", NULL));
    gtk_spin_button_set_value(
        GTK_SPIN_BUTTON(gtk_builder_get_object(curves->builder, "midi-message-resolution")),
        g_key_file_get_integer(key_file, KEY_GROUP, "resolution", NULL));
    gtk_toggle_button_set_active(
        GTK_TOGGLE_BUTTON(gtk_builder_get_object(curves->builder, "toggle-snap")),
        g_key_file_get_boolean(key_file, KEY_GROUP, "snap", NULL));
    gtk_spin_button_set_value(
        GTK_SPIN_BUTTON(gtk_builder_get_object(curves->builder, "grid-x")),
        g_key_file_get_integer(key_file, KEY_GROUP, "grid_x", NULL));
    gtk_spin_button_set_value(
        GTK_SPIN_BUTTON(gtk_builder_get_object(curves->builder, "grid-y")),
        g_key_file_get_integer(key_file, KEY_GROUP, "grid_y", NULL));
    return;
  }
  ui_activate(application, user_data);
}

static void ui_shutdown(GtkApplication *app, gpointer user_data) {
  jack_curves *curves = user_data;
  if (curves->builder != NULL) {
    g_object_unref(curves->builder);
  }
  if (curves->client != NULL) {
    jack_client_close(curves->client);
  }
  if (curves->buffer != NULL) {
    jack_ringbuffer_free(curves->buffer);
  }
  if (curves->edit_points != NULL) {
    g_array_free(curves->edit_points, FALSE);
  }
}

static jack_curves curves_main = {
  .argc = 0,
  .argv = NULL,
  .client = NULL,
  .port = NULL,
  .buffer = NULL,
  .builder = NULL,
  .edit_points = NULL,
  .selected = { .index = -1, .type = POINT_NULL },
  .hovered = { .index = -1, .type = POINT_NULL },
  .play_position = -1.,
  .last_play_position = -1.,
  .handles_visible = false,
  .snapping = true,
  .x_grid = 8,
  .y_grid = 8,
  .time_start = 0,
  .time_end = 0,
  .controller = -1,
  .channel = 0,
  .resolution = 128,
  .num_points = 0,
};

int main (int argc, char **argv) {
  GtkApplication *app;
  int status;

  curves_main.argc = argc;
  curves_main.argv = argv;

  app = gtk_application_new("org.meditator.jack-curves", G_APPLICATION_NON_UNIQUE | G_APPLICATION_HANDLES_OPEN);
  g_signal_connect(app, "activate", G_CALLBACK(ui_activate), &curves_main);
  g_signal_connect(app, "open", G_CALLBACK(ui_open), &curves_main);
  g_signal_connect(app, "shutdown", G_CALLBACK(ui_shutdown), &curves_main);
  status = g_application_run(G_APPLICATION(app), argc, argv);
  g_object_unref(app);

  return status;
}
